/* contains the "business logic"
 */

package com.zuitt.discussion.services;

import com.zuitt.discussion.models.Post;
import com.zuitt.discussion.repositories.PostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

//Will allow us to use CRUD repository methods inherited
@Service
public class PostServiceImpl implements PostService{

    // An object cannot be instantiated from interfaces.
    /*@Autowired allow us to use the interface as if it was
    an instance of an object and allows us to use the methods
    from the CrudRepository
    * */
    @Autowired
    private PostRepository postRepository;

    //Create post
    public void createPost(Post post) {
        postRepository.save(post);
    }

    //Get post
    public  Iterable<Post> getPosts(){
        return postRepository.findAll();
    }
    //delete post
    public ResponseEntity deletePost(Long id){
        postRepository.deleteById(id);
        return new ResponseEntity<>("Post deleted successfully", HttpStatus.OK);
    }
    //Update a Post
    public ResponseEntity updatePost(Long id, Post post) {
        //find the post to update
        Post postForUpdate = postRepository.findById(id).get
                ();
        //Updating the title and content
        postForUpdate.setTitle(post.getTitle());
        postForUpdate.setContent(post.getContent());

        postRepository.save(postForUpdate);

        return  new ResponseEntity<>("Post updated successfully",
                HttpStatus.OK);
    }
}
