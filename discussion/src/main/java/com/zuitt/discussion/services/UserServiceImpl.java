package com.zuitt.discussion.services;



import com.zuitt.discussion.models.User;
import com.zuitt.discussion.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;



@Service
public class UserServiceImpl implements UserService{

    @Autowired
    private UserRepository userRepository;

    //Create user
    public void createUser(User user) { userRepository.save(user);
    }
    //Get all users
    public Iterable<User> getUsers() {return userRepository.findAll();
    }
    //Delete user
    public ResponseEntity deleteUser(Long id) {
        userRepository.deleteById(id);
        return new ResponseEntity<>("User deleted successfully", HttpStatus.OK);
    }

    public ResponseEntity updateUser(Long id, User user) {
        //find the user to update
        User userForUpdate = userRepository.findById(id).get
                ();
        //Updating the username and password
        userForUpdate.setUsername(user.getUsername());
        userForUpdate.setPassword(user.getPassword());

        userRepository.save(userForUpdate);

        return  new ResponseEntity<>("User updated successfully",
                HttpStatus.OK);
    }


}
